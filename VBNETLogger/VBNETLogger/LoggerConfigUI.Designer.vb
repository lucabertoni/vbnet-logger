﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoggerConfigUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoggerConfigUI))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbLogFile = New System.Windows.Forms.TextBox()
        Me.btnESC = New System.Windows.Forms.Button()
        Me.btnSalva = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.nudUpdateFrequency = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnTestLogFile = New System.Windows.Forms.Button()
        Me.nudLogPriority = New System.Windows.Forms.NumericUpDown()
        Me.Label6 = New System.Windows.Forms.Label()
        CType(Me.nudUpdateFrequency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLogPriority, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Underline)
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(138, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Impostazioni base"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.Label3.Location = New System.Drawing.Point(26, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(130, 18)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Percorso Log File:"
        '
        'tbLogFile
        '
        Me.tbLogFile.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbLogFile.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLogFile.Location = New System.Drawing.Point(163, 37)
        Me.tbLogFile.Name = "tbLogFile"
        Me.tbLogFile.Size = New System.Drawing.Size(303, 23)
        Me.tbLogFile.TabIndex = 3
        Me.tbLogFile.Text = ".\log.txt"
        '
        'btnESC
        '
        Me.btnESC.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnESC.BackColor = System.Drawing.Color.IndianRed
        Me.btnESC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnESC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnESC.Image = Global.VBNETLogger.My.Resources.Resources.cross_icon_16
        Me.btnESC.Location = New System.Drawing.Point(329, 147)
        Me.btnESC.Name = "btnESC"
        Me.btnESC.Size = New System.Drawing.Size(114, 45)
        Me.btnESC.TabIndex = 5
        Me.btnESC.Text = "ANNULLA"
        Me.btnESC.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnESC.UseVisualStyleBackColor = False
        '
        'btnSalva
        '
        Me.btnSalva.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSalva.BackColor = System.Drawing.Color.MediumAquamarine
        Me.btnSalva.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalva.Image = Global.VBNETLogger.My.Resources.Resources.save_icon_161
        Me.btnSalva.Location = New System.Drawing.Point(449, 147)
        Me.btnSalva.Name = "btnSalva"
        Me.btnSalva.Size = New System.Drawing.Size(98, 45)
        Me.btnSalva.TabIndex = 4
        Me.btnSalva.Text = "SALVA"
        Me.btnSalva.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnSalva.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.Label4.Location = New System.Drawing.Point(26, 71)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(191, 18)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Intervallo aggiornamento UI:"
        '
        'nudUpdateFrequency
        '
        Me.nudUpdateFrequency.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudUpdateFrequency.Location = New System.Drawing.Point(221, 69)
        Me.nudUpdateFrequency.Maximum = New Decimal(New Integer() {5000, 0, 0, 0})
        Me.nudUpdateFrequency.Name = "nudUpdateFrequency"
        Me.nudUpdateFrequency.Size = New System.Drawing.Size(65, 24)
        Me.nudUpdateFrequency.TabIndex = 7
        Me.nudUpdateFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudUpdateFrequency.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.Label5.Location = New System.Drawing.Point(286, 71)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 18)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "ms"
        '
        'btnTestLogFile
        '
        Me.btnTestLogFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTestLogFile.BackColor = System.Drawing.Color.Peru
        Me.btnTestLogFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTestLogFile.Location = New System.Drawing.Point(472, 37)
        Me.btnTestLogFile.Name = "btnTestLogFile"
        Me.btnTestLogFile.Size = New System.Drawing.Size(75, 23)
        Me.btnTestLogFile.TabIndex = 9
        Me.btnTestLogFile.Text = "TEST"
        Me.btnTestLogFile.UseVisualStyleBackColor = False
        '
        'nudLogPriority
        '
        Me.nudLogPriority.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudLogPriority.Location = New System.Drawing.Point(227, 103)
        Me.nudLogPriority.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nudLogPriority.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudLogPriority.Name = "nudLogPriority"
        Me.nudLogPriority.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.nudLogPriority.Size = New System.Drawing.Size(42, 24)
        Me.nudLogPriority.TabIndex = 11
        Me.nudLogPriority.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudLogPriority.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.Label6.Location = New System.Drawing.Point(26, 105)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(198, 18)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Mostra log con priorità >= di:"
        '
        'LoggerConfigUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(561, 199)
        Me.Controls.Add(Me.nudLogPriority)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnTestLogFile)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.nudUpdateFrequency)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnESC)
        Me.Controls.Add(Me.btnSalva)
        Me.Controls.Add(Me.tbLogFile)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "LoggerConfigUI"
        Me.Text = "Logger - Configurazione"
        CType(Me.nudUpdateFrequency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLogPriority, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbLogFile As System.Windows.Forms.TextBox
    Friend WithEvents btnESC As System.Windows.Forms.Button
    Friend WithEvents btnSalva As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents nudUpdateFrequency As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnTestLogFile As System.Windows.Forms.Button
    Friend WithEvents nudLogPriority As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class
