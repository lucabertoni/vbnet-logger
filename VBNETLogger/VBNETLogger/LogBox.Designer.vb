﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LogBox
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnEraser = New System.Windows.Forms.Button()
        Me.btnSetting = New System.Windows.Forms.Button()
        Me.btnLog = New System.Windows.Forms.Button()
        Me.btnFilterMessage = New System.Windows.Forms.Button()
        Me.rtb_logs = New System.Windows.Forms.RichTextBox()
        Me.btnFilterError = New System.Windows.Forms.Button()
        Me.btnFilterInfo = New System.Windows.Forms.Button()
        Me.TimerLogUpdate = New System.Windows.Forms.Timer(Me.components)
        Me.btnFilterWarning = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnEraser
        '
        Me.btnEraser.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEraser.BackColor = System.Drawing.Color.Peru
        Me.btnEraser.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown
        Me.btnEraser.FlatAppearance.BorderSize = 2
        Me.btnEraser.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEraser.Image = Global.VBNETLogger.My.Resources.Resources.eraser_icon_25
        Me.btnEraser.Location = New System.Drawing.Point(720, 142)
        Me.btnEraser.Name = "btnEraser"
        Me.btnEraser.Size = New System.Drawing.Size(45, 40)
        Me.btnEraser.TabIndex = 45
        Me.btnEraser.UseVisualStyleBackColor = False
        '
        'btnSetting
        '
        Me.btnSetting.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSetting.BackColor = System.Drawing.Color.Peru
        Me.btnSetting.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown
        Me.btnSetting.FlatAppearance.BorderSize = 2
        Me.btnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetting.Image = Global.VBNETLogger.My.Resources.Resources.setting_icon_25
        Me.btnSetting.Location = New System.Drawing.Point(720, 50)
        Me.btnSetting.Name = "btnSetting"
        Me.btnSetting.Size = New System.Drawing.Size(45, 40)
        Me.btnSetting.TabIndex = 44
        Me.btnSetting.UseVisualStyleBackColor = False
        '
        'btnLog
        '
        Me.btnLog.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLog.BackColor = System.Drawing.Color.Peru
        Me.btnLog.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown
        Me.btnLog.FlatAppearance.BorderSize = 2
        Me.btnLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLog.Image = Global.VBNETLogger.My.Resources.Resources.file_icon_32
        Me.btnLog.Location = New System.Drawing.Point(720, 96)
        Me.btnLog.Name = "btnLog"
        Me.btnLog.Size = New System.Drawing.Size(45, 40)
        Me.btnLog.TabIndex = 43
        Me.btnLog.UseVisualStyleBackColor = False
        '
        'btnFilterMessage
        '
        Me.btnFilterMessage.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilterMessage.BackColor = System.Drawing.Color.IndianRed
        Me.btnFilterMessage.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.btnFilterMessage.FlatAppearance.BorderSize = 2
        Me.btnFilterMessage.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed
        Me.btnFilterMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFilterMessage.Image = Global.VBNETLogger.My.Resources.Resources.message_icon_25
        Me.btnFilterMessage.Location = New System.Drawing.Point(639, 8)
        Me.btnFilterMessage.Name = "btnFilterMessage"
        Me.btnFilterMessage.Size = New System.Drawing.Size(34, 30)
        Me.btnFilterMessage.TabIndex = 49
        Me.btnFilterMessage.UseVisualStyleBackColor = False
        '
        'rtb_logs
        '
        Me.rtb_logs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtb_logs.BackColor = System.Drawing.Color.Black
        Me.rtb_logs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.rtb_logs.Font = New System.Drawing.Font("Gadugi", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtb_logs.ForeColor = System.Drawing.Color.Peru
        Me.rtb_logs.Location = New System.Drawing.Point(3, 46)
        Me.rtb_logs.Name = "rtb_logs"
        Me.rtb_logs.ReadOnly = True
        Me.rtb_logs.RightMargin = 700
        Me.rtb_logs.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
        Me.rtb_logs.Size = New System.Drawing.Size(787, 230)
        Me.rtb_logs.TabIndex = 42
        Me.rtb_logs.Text = ""
        '
        'btnFilterError
        '
        Me.btnFilterError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilterError.BackColor = System.Drawing.Color.IndianRed
        Me.btnFilterError.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.btnFilterError.FlatAppearance.BorderSize = 2
        Me.btnFilterError.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed
        Me.btnFilterError.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFilterError.Image = Global.VBNETLogger.My.Resources.Resources.error_icon_25
        Me.btnFilterError.Location = New System.Drawing.Point(748, 8)
        Me.btnFilterError.Name = "btnFilterError"
        Me.btnFilterError.Size = New System.Drawing.Size(34, 30)
        Me.btnFilterError.TabIndex = 48
        Me.btnFilterError.UseVisualStyleBackColor = False
        '
        'btnFilterInfo
        '
        Me.btnFilterInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilterInfo.BackColor = System.Drawing.Color.IndianRed
        Me.btnFilterInfo.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.btnFilterInfo.FlatAppearance.BorderSize = 2
        Me.btnFilterInfo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed
        Me.btnFilterInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFilterInfo.Image = Global.VBNETLogger.My.Resources.Resources.info_icon_25
        Me.btnFilterInfo.Location = New System.Drawing.Point(675, 8)
        Me.btnFilterInfo.Name = "btnFilterInfo"
        Me.btnFilterInfo.Size = New System.Drawing.Size(34, 30)
        Me.btnFilterInfo.TabIndex = 47
        Me.btnFilterInfo.UseVisualStyleBackColor = False
        '
        'TimerLogUpdate
        '
        Me.TimerLogUpdate.Enabled = True
        '
        'btnFilterWarning
        '
        Me.btnFilterWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilterWarning.BackColor = System.Drawing.Color.IndianRed
        Me.btnFilterWarning.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.btnFilterWarning.FlatAppearance.BorderSize = 2
        Me.btnFilterWarning.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed
        Me.btnFilterWarning.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFilterWarning.Image = Global.VBNETLogger.My.Resources.Resources.warning_icon_25
        Me.btnFilterWarning.Location = New System.Drawing.Point(711, 8)
        Me.btnFilterWarning.Name = "btnFilterWarning"
        Me.btnFilterWarning.Size = New System.Drawing.Size(34, 30)
        Me.btnFilterWarning.TabIndex = 46
        Me.btnFilterWarning.UseVisualStyleBackColor = False
        '
        'LogBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnEraser)
        Me.Controls.Add(Me.btnSetting)
        Me.Controls.Add(Me.btnLog)
        Me.Controls.Add(Me.btnFilterMessage)
        Me.Controls.Add(Me.rtb_logs)
        Me.Controls.Add(Me.btnFilterError)
        Me.Controls.Add(Me.btnFilterInfo)
        Me.Controls.Add(Me.btnFilterWarning)
        Me.Name = "LogBox"
        Me.Size = New System.Drawing.Size(793, 285)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnEraser As System.Windows.Forms.Button
    Friend WithEvents btnSetting As System.Windows.Forms.Button
    Friend WithEvents btnLog As System.Windows.Forms.Button
    Friend WithEvents btnFilterMessage As System.Windows.Forms.Button
    Friend WithEvents rtb_logs As System.Windows.Forms.RichTextBox
    Friend WithEvents btnFilterError As System.Windows.Forms.Button
    Friend WithEvents btnFilterInfo As System.Windows.Forms.Button
    Friend WithEvents btnFilterWarning As System.Windows.Forms.Button
    Public WithEvents TimerLogUpdate As System.Windows.Forms.Timer

End Class
