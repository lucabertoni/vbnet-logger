﻿Public Class VBLoggerFactory
    Private Shared _logger_intitialized As Boolean = False
    Private Shared _logger As VBLogger

    Public Shared Function Logger() As VBLogger
        If Not VBLoggerFactory._logger_intitialized Then
            VBLoggerFactory._logger = New VBLogger()
            VBLoggerFactory._logger_intitialized = True

            VBLoggerFactory._logger.Log("Initialized logger at " + DateAndTime.Now.ToUniversalTime, VBLogger.LOG_PRIORITIES.PRIORITY_LOWER)
            VBLoggerFactory._logger.Log("--------------------------------------------------", VBLogger.LOG_PRIORITIES.PRIORITY_LOWER)
        End If

        Return VBLoggerFactory._logger
    End Function

    Public Shared Function Dispose() As Boolean
        Try
            VBLoggerFactory._logger.Dispose()

            VBLoggerFactory._logger_intitialized = False

            Return True
        Catch ex As Exception

        End Try

        VBLoggerFactory._logger_intitialized = False
        Return False
    End Function

End Class
