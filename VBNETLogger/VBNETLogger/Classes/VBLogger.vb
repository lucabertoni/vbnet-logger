Imports Microsoft.VisualBasic
Imports System.Windows.Forms
Imports System.IO
Imports System.Text
Imports System.Diagnostics
Imports System.Threading
Imports System.Text.RegularExpressions

Public Class VBLogger
    Implements System.IDisposable
    Public Enum LOG_TYPES
        LOG_MESSAGE_BOX = 0
        LOG_INFO
        LOG_WARNING
        LOG_ERROR
        LOG_FREE
        LOGGER_ERROR
        _LOG_TYPE_LAST
    End Enum

    Public Enum LOG_PRIORITIES
        PRIORITY_LOWER = 1
        PRIORITY_MIDDLE = 5
        PRIORITY_HIGHER = 10
    End Enum

    Public error_no As LOGGER_ERRORS

    Public LOG_TYPES_LABELS() As String = {
        "MESSAGE",
        "INFO",
        "WARNING",
        "ERROR",
        "LOG",
        "FATAL LOGGER ERROR"
    }

    Public Enum LOG_ENABLED
        ENABLED
        DISABLED
    End Enum


    Private _log_file As String = Application.StartupPath() + "\log.txt"
    Public _config_file As String = Application.StartupPath() + "\logger.conf"
    Public _log_file_stream As StreamWriter
    Public _show_message_box_logs = True ' Show MessageBox(es) when messagebox log is selected
    Private _log_line_prefix = ""   ' Text appended after date-time in logs

    Private _log_lines_max As Integer = 100000

    Private _logs(_log_lines_max - 1) As String
    Private _logs_first_free_stack_cell_pointer As Integer = (_log_lines_max - 1)

    Private _rtb_logs(_log_lines_max - 1) As String
    Private _rtb_logs_first_free_stack_cell_pointer As Integer = (_log_lines_max - 1)
    Private _rtb_logs_pointer_locked = False

    Private _logs_file_stack(_log_lines_max - 1) As String
    Private _logs_file_first_free_stack_cell_pointer As Integer = (_log_lines_max - 1)
    Private _lock_logs_file_stack_pointer As Boolean = False

    Private _notepad_log_process As System.Diagnostics.Process = Nothing
    Private _file_logging_stopped As Boolean = False
    Private _file_logging_thread As Thread = Nothing

    Public _ui_update_frequency_ms As Integer = 100
    Public _ui_log_priority As Integer = 1
    Private _ui_thread As Thread = Nothing

    Private _filter_log_type_bitmask(VBLogger.LOG_TYPES._LOG_TYPE_LAST) As Integer
    Private _filter_date_range(2) As Date

    Private _dispose As Boolean = False

    Public Enum LOGGER_ERRORS
        ERROR_CANNOT_ACCESS_LOG_FILE
        NO_ERROR
    End Enum

    Public Function loadConfig() As Boolean
        Dim config_line As String = ""
        Dim loaded As Boolean = False

        Try
            Dim reader As StreamReader = New StreamReader(Me._config_file)

            While Not reader.EndOfStream
                config_line = reader.ReadLine()

                If config_line.Contains("log_file=") Then
                    Me._log_file = config_line.Substring(config_line.IndexOf("log_file=") + "log_file=".Length).Trim
                ElseIf config_line.Contains("ui_update_frequency_ms=") Then
                    Me._ui_update_frequency_ms = CInt(config_line.Substring(config_line.IndexOf("ui_update_frequency_ms=") + "ui_update_frequency_ms=".Length).Trim())
                ElseIf config_line.Contains("ui_log_priority=") Then
                    Me._ui_log_priority = CInt(config_line.Substring(config_line.IndexOf("ui_log_priority=") + "ui_log_priority=".Length).Trim())
                End If
            End While

            reader.Close()
            loaded = True
        Catch ex As Exception
            Me._log_file = Application.StartupPath() + "\log.txt"
            Me._ui_update_frequency_ms = 100
            Me._ui_log_priority = 1

            loaded = True
        End Try

        Return loaded
    End Function

    Public Sub New()
        If Not Me.loadConfig() Then
            Me.Log("Si sono verificati degli errori durante la configurazione del logger. Verificare che il file di configurazione " + Me._config_file + " sia accessibile ed effettuare test di scrittura del file di log dal pannello di configurazione. Verranno pertanto utilizzate delle configurazioni base.", LOG_PRIORITIES.PRIORITY_HIGHER)
            Exit Sub
        Else
            Try
                Me._log_file_stream = New StreamWriter(Me._log_file, True)
            Catch e As IOException
                Me.error_no = LOGGER_ERRORS.ERROR_CANNOT_ACCESS_LOG_FILE
            Catch e As UnauthorizedAccessException
                Me.error_no = LOGGER_ERRORS.ERROR_CANNOT_ACCESS_LOG_FILE
            End Try
        End If

        Me._filter_log_type_bitmask = {1, 1, 1, 1}

        Me._dispose = False
        Me._file_logging_stopped = False
    End Sub

    Public Function GetLogFilePath() As String
        Return Me._log_file
    End Function

    Private Function LogStackPointer() As Integer
        Dim old_pointer As Integer = Me._logs_first_free_stack_cell_pointer

        If Me._logs_first_free_stack_cell_pointer <= 1 Then
            Me._logs_first_free_stack_cell_pointer = (_log_lines_max - 1)
        Else
            Me._logs_first_free_stack_cell_pointer -= 1
        End If

        Return old_pointer
    End Function

    Private Sub WaitRTBPointer()
        While _rtb_logs_pointer_locked
        End While
    End Sub

    Private Function RtbLogStackPointer() As Integer
        Me.WaitRTBPointer()

        Me._rtb_logs_pointer_locked = True
        Dim old_pointer As Integer = Me._rtb_logs_first_free_stack_cell_pointer

        If Me._rtb_logs_first_free_stack_cell_pointer <= 1 Then
            Me._rtb_logs_first_free_stack_cell_pointer = (_log_lines_max - 1)
        Else
            Me._rtb_logs_pointer_locked = True
            Me._rtb_logs_first_free_stack_cell_pointer -= 1
            Me._rtb_logs_pointer_locked = False
        End If

        Me._rtb_logs_pointer_locked = False
        Return old_pointer
    End Function

    ' Resetta il puntatore allo stack dei log da scrivere su file in modo da puntare al primo valore da scrivere
    Private Function ResetLogFileStackPointer() As Integer
        Me._logs_file_first_free_stack_cell_pointer = (_log_lines_max - 1)
        Return Me._logs_file_first_free_stack_cell_pointer
    End Function

    Private Function LogFileStackPointer() As Integer
        Dim old_pointer As Integer = Me._logs_file_first_free_stack_cell_pointer

        Me._lock_logs_file_stack_pointer = True

        If Me._logs_file_first_free_stack_cell_pointer <= 1 Then
            Me._logs_file_first_free_stack_cell_pointer = (_log_lines_max - 1)
        Else
            Me._logs_file_first_free_stack_cell_pointer -= 1
        End If

        Me._lock_logs_file_stack_pointer = False

        Return old_pointer
    End Function

    Public Function Configure(Optional ByVal log_file As String = ".\log.txt", Optional ByVal priority As Integer = 1) As Integer
        Me.loadConfig()

        Me._log_file = log_file
        Me._ui_log_priority = priority
        Return -1
    End Function

    Private Sub _LogRotate()
        Dim log_file As New FileInfo(Me._log_file)
        If log_file.Length > 10000000 Then
            Dim dest_dir As String = Me._log_file.Substring(0, IIf(Me._log_file.LastIndexOf("\") > 0, Me._log_file.LastIndexOf("\"), 0)) + "\LOG\"

            If Not Directory.Exists(dest_dir) Then
                Try
                    ' Controllo se la cartella esiste altrimenti la creo
                    Directory.CreateDirectory(dest_dir)
                Catch ex As Exception
                End Try
            End If

            Try
                File.Move(Me._log_file, dest_dir + "log_" + DateTime.Now.Year.ToString + DateTime.Now.Month.ToString + DateTime.Now.Day.ToString + "_" + DateTime.Now.Hour.ToString + DateTime.Now.Minute.ToString + ".txt")
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub _LogStackToFile()
        Dim log_text As String

        While _file_logging_stopped
            Try
                _file_logging_stopped = Not Me._notepad_log_process.HasExited()

                If Me._dispose Then
                    Thread.CurrentThread.Abort()
                End If

            Catch ex As Exception
            End Try
        End While

        If Not (Me.error_no = LOGGER_ERRORS.ERROR_CANNOT_ACCESS_LOG_FILE) Then
            While True ' Infinite loop
                If Me._dispose Then
                    Thread.CurrentThread.Abort()
                End If

                Try
                    log_text = Me._logs_file_stack(Me._log_lines_max - 1)

                    If Not (log_text = "") Then
                        Try
                            Me._log_file_stream.Close()
                        Catch ex As Exception
                        End Try

                        Me._LogRotate()
                        Try
                            Me._log_file_stream = New StreamWriter(Me._log_file, True)

                            _file_logging_stopped = True
                        Catch ex As Exception
                            'Me.Log(LOG_TYPES.LOGGER_ERROR, LOG_PRIORITIES.PRIORITY_MIDDLE, "Impossibile scrivere log su file. Il file " + Me._log_file + " risulta inaccessibile")


                            Me.error_no = LOGGER_ERRORS.ERROR_CANNOT_ACCESS_LOG_FILE

                            Thread.Sleep(500)

                            Continue While
                        End Try

                        Me._log_file_stream.WriteLine(log_text)

                        Me._log_file_stream.Close()

                        ' Shifting elemets
                        For index = (Me._log_lines_max - 1) To 1 Step -1
                            If IsNothing(Me._logs_file_stack(index)) Or (Me._logs_file_stack(index) = "") Then

                                Me._lock_logs_file_stack_pointer = True
                                Me._logs_file_first_free_stack_cell_pointer = IIf(index = (Me._log_lines_max - 1), index, (index + 1))
                                Me._lock_logs_file_stack_pointer = False

                                Exit For
                            End If

                            Me._logs_file_stack(index) = IIf(IsNothing(Me._logs_file_stack(index - 1)), "", Me._logs_file_stack(index - 1))
                        Next
                    End If

                    _file_logging_stopped = False
                Catch ex As Exception
                    Me.Log(VBLogger.LOG_TYPES.LOGGER_ERROR, LOG_PRIORITIES.PRIORITY_HIGHER, "Errore scrittura log su file")
                    Me.error_no = LOGGER_ERRORS.ERROR_CANNOT_ACCESS_LOG_FILE

                    Thread.Sleep(2000)
                End Try


                Try
                    Me._log_file_stream.Close()
                Catch ex As Exception
                End Try

            End While
        End If
    End Sub

    Private Function _LogToFile() As Boolean
        Try
            If IsNothing(Me._file_logging_thread) Then
                Me._file_logging_thread = New Thread(AddressOf _LogStackToFile)
                Me._file_logging_thread.Name = "DF File logging"
                Me._file_logging_thread.Start()
            Else
                If Not (Me._file_logging_thread.IsAlive()) Then
                    Me._file_logging_thread = New Thread(AddressOf _LogStackToFile)
                    Me._file_logging_thread.Name = "DF File logging"
                    Me._file_logging_thread.Start()
                End If
            End If
        Catch ex As Exception
        End Try

        Return False
    End Function

    ' Cosa fa           :           Aggiunge un log allo stack e lo stampa nel file di log
    Private Function _Log(ByVal log_type As LOG_TYPES, ByVal priority As Integer, ByVal text As String, ByVal exception As Exception, ByVal message_box_title As String, ByVal message_box_buttons As MessageBoxButtons, ByVal message_box_icon As MessageBoxIcon) As Boolean
        Dim log As String
        Dim log_date As Date = DateTime.Now
        Dim exception_message As String = ""

        If Not (IsNothing(exception)) Then
            exception_message = exception.Message
        End If

        Me.error_no = LOGGER_ERRORS.NO_ERROR

        log = (String.Format("[{0}][{1:7}][{2:18}] {3} {4}",
                                    CStr(priority),
                                    LOG_TYPES_LABELS(CInt(log_type)),
                                    log_date.ToString,
                                    text,
                                    CStr(IIf(Not IsNothing(exception), "| Eccezione generata: " + exception_message, ""))
                                  )
                    ).ToString

        Me._logs(Me.LogStackPointer) = log

        If Not (VBLogger.FilterLog(log, Me._filter_date_range, "", Me._ui_log_priority, Me._filter_log_type_bitmask) = "") Then
            ' Aggiungo il log allo stack dei log da scrivere nel file di log
            If Not (IsNothing(Me._rtb_logs)) Then
                Me._rtb_logs(Me.RtbLogStackPointer) = log ' Necessario per visualizzazione a schermo dei log. Me._rtb_logs viene resettato alla chiusura dell'UI a differenza di Me._logs che permane in memoria
            End If
        End If

        Me._logs_file_stack(Me.LogFileStackPointer) = log

        If IsNothing(Me._file_logging_thread) Then
            Me._file_logging_thread = New Thread(AddressOf _LogStackToFile)
            Me._file_logging_thread.Name = "DF File log thread"
            Me._file_logging_thread.Start()
        Else
            If Not (Me._file_logging_thread.IsAlive()) Then
                Me._file_logging_thread = New Thread(AddressOf _LogStackToFile)
                Me._file_logging_thread.Name = "DF File log thread"
                Me._file_logging_thread.Start()
            End If
        End If

        If log_type = LOG_TYPES.LOG_MESSAGE_BOX And Me._show_message_box_logs Then
            MessageBox.Show(log.Substring(log.IndexOf("]") + 1), message_box_title, message_box_buttons, message_box_icon)
        End If

        Return (Me.error_no.Equals(LOGGER_ERRORS.NO_ERROR))
    End Function
    Public Function Log(ByVal log_text As String, ByVal priority As Integer) As Boolean
        Return Me._Log(VBLogger.LOG_TYPES.LOG_FREE, priority, log_text, Nothing, Nothing, Nothing, Nothing)
    End Function

    Public Function Log(ByVal log_type As LOG_TYPES, ByVal priority As Integer, ByVal text As String) As Boolean
        Return Me._Log(log_type, priority, text, Nothing, Nothing, Nothing, Nothing)
    End Function

    Public Function Log(ByVal log_type As LOG_TYPES, ByVal priority As Integer, ByVal text As String, ByVal exception As Exception) As Boolean
        Return Me._Log(log_type, priority, text, exception, Nothing, Nothing, Nothing)
    End Function

    Public Function Log(ByVal log_type As LOG_TYPES, ByVal priority As Integer, ByVal text As String, ByVal exception As Exception, ByVal message_box_title As String, ByVal message_box_buttons As MessageBoxButtons, ByVal message_box_icon As MessageBoxIcon) As Boolean
        Return Me._Log(log_type, priority, text, exception, message_box_title, message_box_buttons, message_box_icon)
    End Function

    ' Log disabilitati
    Public Function Log(ByVal log_disabled As LOG_ENABLED, ByVal log_text As String, ByVal priority As Integer) As Boolean
        If log_disabled = LOG_ENABLED.ENABLED Then
            Return Me._Log(VBLogger.LOG_TYPES.LOG_FREE, priority, log_text, Nothing, Nothing, Nothing, Nothing)
        Else
            Return True
        End If
    End Function

    Public Function Log(ByVal log_disabled As LOG_ENABLED, ByVal log_type As LOG_TYPES, ByVal priority As Integer, ByVal text As String) As Boolean
        If log_disabled = LOG_ENABLED.ENABLED Then
            Return Me._Log(log_type, priority, text, Nothing, Nothing, Nothing, Nothing)
        Else
            Return True
        End If
    End Function

    Public Function Log(ByVal log_disabled As LOG_ENABLED, ByVal log_type As LOG_TYPES, ByVal priority As Integer, ByVal text As String, ByVal exception As Exception) As Boolean
        If log_disabled = LOG_ENABLED.ENABLED Then
            Return Me._Log(log_type, priority, text, exception, Nothing, Nothing, Nothing)
        Else
            Return True
        End If
    End Function

    Public Function Log(ByVal log_disabled As LOG_ENABLED, ByVal log_type As LOG_TYPES, ByVal priority As Integer, ByVal text As String, ByVal exception As Exception, ByVal message_box_title As String, ByVal message_box_buttons As MessageBoxButtons, ByVal message_box_icon As MessageBoxIcon) As Boolean
        If log_disabled = LOG_ENABLED.ENABLED Then
            Return Me._Log(log_type, priority, text, exception, message_box_title, message_box_buttons, message_box_icon)
        Else
            Return True
        End If
    End Function

    ' Cosa fa           :           Ottiene un log dallo stack rimuovendolo
    Public Function GetLog() As String
        Dim return_log As String = ""

        If Me._logs.Length > 0 Then
            return_log = Me._logs(Me._logs.Length - 1)

            ' Shifting elemets
            For index = (Me._log_lines_max - 1) To 0 Step -1
                If IsNothing(Me._logs(index)) Or (Me._logs(index) = "") Then
                    Me._logs_first_free_stack_cell_pointer = IIf(index = (Me._log_lines_max - 1), index, (index + 1))

                    Exit For
                End If

                Me._logs(index) = IIf(IsNothing(Me._logs(index - 1)), "", Me._logs(index - 1))
            Next
        End If

        Return return_log
    End Function
    ' Cosa fa           :           Ottiene un log dallo stack rimuovendolo
    Public Function GetUILog() As String
        Dim return_log As String = ""

        If Me._rtb_logs.Length > 0 Then
            Me.WaitRTBPointer()

            Me._rtb_logs_pointer_locked = True
            return_log = Me._rtb_logs(Me._rtb_logs.Length - 1)

            ' Shifting elemets
            If Not (return_log = "") Then
                For index = (Me._log_lines_max - 1) To 1 Step -1
                    If IsNothing(Me._rtb_logs(index)) Or (Me._rtb_logs(index) = "") Then
                        Me._rtb_logs_first_free_stack_cell_pointer = IIf(index = (Me._log_lines_max - 1), index, (index + 1))

                        Exit For
                    End If

                    Me._rtb_logs(index) = IIf(IsNothing(Me._rtb_logs(index - 1)), "", Me._rtb_logs(index - 1))
                Next
            End If
        End If

        Me._rtb_logs_pointer_locked = False
        Return return_log
    End Function

    Private Sub _ShowUI()
        Dim form As New LoggerMainUI(Me)
        form.ShowDialog()
    End Sub

    Public Sub InitializeUI()
    End Sub


    Public Sub ShowUI()
        Try
            Me._ui_thread.Abort()

            Me._ui_thread = Nothing
        Catch ex As Exception
        End Try

        Try
            Me._ui_thread = New Thread(AddressOf _ShowUI)
            Me._ui_thread.Name = "LoggerUI Thread"
            Me._ui_thread.Start()
        Catch ex As Exception
            Me.Log(LOG_TYPES.LOGGER_ERROR, LOG_PRIORITIES.PRIORITY_HIGHER, "Errore creazione thread UI")
        End Try
    End Sub

    Public Sub CloseUI()
        Try
            Me._ui_thread.Abort()
        Catch ex As Exception
        End Try

        Me._ui_thread = Nothing
    End Sub

    Public Sub OpenLogFile()
        Try
            Me._log_file_stream.Close()

            If System.IO.File.Exists(Me._log_file) = True Then
                Me._notepad_log_process = Process.Start(Me._log_file)

                Me._file_logging_stopped = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    ' Use carefully
    Public Sub StopFileLogging()
        Me._file_logging_stopped = False

        Try
            Me._log_file_stream.Close()
        Catch ex As Exception
        End Try
    End Sub

    ' Use carefully
    Public Sub StartFileLogging()
        Me._file_logging_stopped = True

        Try
            Me._log_file_stream = New StreamWriter(Me._log_file, True)
        Catch ex As Exception
            Me.Log(LOG_TYPES.LOGGER_ERROR, LOG_PRIORITIES.PRIORITY_HIGHER, "Errore apertura file di log. Potrebbe già essere stato aperto dal thread." + Me._log_file, ex)
        End Try
    End Sub

    Public Sub SetFilterDateRange(ByVal date_range() As Date)
        Me._filter_date_range = date_range
    End Sub

    Public Sub SetFilterLogBitMask(ByVal bitmask() As Integer)
        Me._filter_log_type_bitmask = bitmask
    End Sub

    ' Cosa fa               :           Applica i filtri al log per decidere se mostrarlo oppure no
    ' log_text              :           stringa, testo log
    ' filter_date_range     :           array(2), range di date, data inizio e data fine
    ' filter_regex          :           filtro regex
    ' filter_priority       :           int, priorità del filtro
    ' Ritorna               :           return_log -> stringa, "" = log filtrato | log_text = il log può essere mostrato
    Public Shared Function FilterLog(ByVal log_text As String, ByVal filter_date_range() As Date, ByVal filter_regex As String, ByVal filter_priority As Integer, ByVal filter_log_type_bitmask() As Integer) As String
        Dim log_date_string, regex_string, log_types_filter As String
        Dim date_start, date_end, log_date As Date
        Dim log_priority As Integer
        Dim return_log As String = ""

        return_log = log_text

        If Not (log_text = "") Then
            return_log = ""

            'If Me.cB_date_filter.Checked Then
            If Not (IsNothing(filter_date_range)) Then
                ' Filtro per range di date
                log_date_string = (log_text.Substring(GetNthIndex(log_text, "[", 3) + 1, 19))

                log_date = CDate(log_date_string)

                If filter_date_range.Length = 2 Then
                    date_start = filter_date_range(0)
                    date_end = filter_date_range(1)

                    If Not ((DateTime.Compare(log_date.Date, date_start.Date) >= 0) And (DateTime.Compare(log_date.Date, date_end.Date) <= 0)) Then
                        Return return_log
                    End If
                End If
            End If

            ' If cb_Filter_regex.Checked Then
            If Not (filter_regex = "") Then
                regex_string = filter_regex
            Else
                log_types_filter = (VBLoggerFactory.Logger.LOG_TYPES_LABELS(VBLogger.LOG_TYPES.LOGGER_ERROR) + "|" _
                                    + VBLoggerFactory.Logger.LOG_TYPES_LABELS(VBLogger.LOG_TYPES.LOG_FREE) + "|" _
                                             + IIf(filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX) = 1, VBLoggerFactory.Logger.LOG_TYPES_LABELS(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX) + "|", "") _
                                             + IIf(filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_INFO) = 1, VBLoggerFactory.Logger.LOG_TYPES_LABELS(VBLogger.LOG_TYPES.LOG_INFO) + "|", "") _
                                             + IIf(filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_WARNING) = 1, VBLoggerFactory.Logger.LOG_TYPES_LABELS(VBLogger.LOG_TYPES.LOG_WARNING) + "|", "") _
                                             + IIf(filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_ERROR) = 1, VBLoggerFactory.Logger.LOG_TYPES_LABELS(VBLogger.LOG_TYPES.LOG_ERROR) + "|", "") _
                                             )

                log_types_filter = log_types_filter.Substring(0, log_types_filter.Length - 1)

                regex_string = String.Format("^\[\d+\]\[({0}){1}", log_types_filter, ".+"
                                            ) '[NN] = PRIORITA' + {0} = "FATAL LOGGER ERROR|MESSAGE|INFO|WARNING|ERROR" + {1} = ".+"
            End If ' Regex Filter

            log_priority = CInt(log_text.Substring(1, log_text.IndexOf("]") - 1))

            'Try
            If New Regex(regex_string).Match(log_text).Success And (log_priority >= filter_priority) Then
                return_log = log_text.Substring(log_text.IndexOf("]") + 1) ' Rimuovo la priorità
            End If
            'Catch ex As Exception
            'End Try
        End If

        Return return_log
    End Function

    Public Sub Dispose() Implements System.IDisposable.Dispose
        Me._dispose = True

        ' Rimuovo i log pendenti
        Erase Me._rtb_logs

        Try
            Me._log_file_stream.Close()
        Catch ex As Exception
        End Try

        Try
            Me._file_logging_thread.Abort()
        Catch ex As Exception

        End Try

    End Sub

    Protected Overrides Sub Finalize()
        Dispose()
    End Sub
End Class
