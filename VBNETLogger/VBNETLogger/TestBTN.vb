﻿Imports System.Windows.Forms

Public Class TestBTN

    Private Sub btnOpenLogger_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        Dim priority As Integer

        VBLoggerFactory.Logger.Configure("C:\test_log.txt", 6)

        VBLoggerFactory.Logger.ShowUI()

        For index = 1 To 30000
            priority = CInt(Math.Ceiling(Rnd() * 10)) + 1
            VBLoggerFactory.Logger.Log("Messaggio libero|Priorità: " + priority.ToString, priority)
        Next

        For index = 1 To 30000
            priority = CInt(Math.Ceiling(Rnd() * 10)) + 1
            VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_INFO, priority, "Log info|Priorità: " + priority.ToString)
        Next

        'For index = 1 To 30000
        '    priority = CInt(Math.Ceiling(Rnd() * 10)) + 1
        '    VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_WARNING, priority, "Log warning|Priorità: " + priority.ToString)
        'Next

        'For index = 1 To 30000
        '    priority = CInt(Math.Ceiling(Rnd() * 10)) + 1
        '    VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_ERROR, priority, "Log error|Priorità: " + priority.ToString)
        'Next

        'VBLoggerFactory.Logger._show_message_box_logs = False
        'For index = 1 To 30000
        '    priority = CInt(Math.Ceiling(Rnd() * 10)) + 1
        '    VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX, priority, "Log message|Priorità: " + priority.ToString)
        'Next

        MessageBox.Show("Test completato", "Test completato", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
End Class
