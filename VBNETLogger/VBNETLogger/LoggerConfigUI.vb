﻿Imports System.IO
Imports System.Windows.Forms
Imports System.Threading

Public Class LoggerConfigUI
    Private _logger As VBLogger ' Istanza corrente del logger

    Public Sub New(ByRef logger As VBLogger)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._logger = logger

        Me.tbLogFile.Text = Me._logger.GetLogFilePath()
        Me.nudUpdateFrequency.Value = Me._logger._ui_update_frequency_ms
        Me.nudLogPriority.Value = Me._logger._ui_log_priority
    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub
    Private Function testLogFile() As Boolean
        Dim ret As Boolean = False
        Dim log_file_path As String = Me.tbLogFile.Text.Trim

        Me._logger.StopFileLogging()

        Thread.Sleep(3000)
        Try
            Dim writer As StreamWriter = New StreamWriter(log_file_path, True)

            writer.WriteLine("[1][INFO][" + DateTime.Now.ToString + "] Test scrittura file di log")

            writer.Close()
            ret = True
        Catch ex As Exception
        End Try

        Me._logger.StartFileLogging()
        Return ret
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnTestLogFile.Click
        ' Interrompo i log su file intanto che configuro

        If Not Me.testLogFile() Then
            MessageBox.Show("Si è verificato un errore durante il test di scrittura sul file di log. Pertanto non sarà possibile salvare i log su file. E' necessario specificare il nome di un file accessibile all'utente che esegue questo software.", "Errore scrittura file di log", MessageBoxButtons.OK, MessageBoxIcon.Error)
            btnSalva.Enabled = False
        Else
            MessageBox.Show("Test di scrittura effettuato correttamente", "Test scrittura riuscito", MessageBoxButtons.OK, MessageBoxIcon.Information)
            btnSalva.Enabled = True
        End If
    End Sub

    Private Function saveConfiguration() As Boolean
        Dim saved As Boolean = False

        If Me.testLogFile() Then
            Try
                Dim writer As StreamWriter = New StreamWriter(Me._logger._config_file, False)

                ' Scrivo log
                writer.WriteLine("[BASE]")
                writer.WriteLine("log_file=" + Me.tbLogFile.Text)
                writer.WriteLine("ui_update_frequency_ms=" + Me.nudUpdateFrequency.Value.ToString)
                writer.WriteLine("ui_log_priority=" + Me.nudLogPriority.Value.ToString)

                writer.Close()

                saved = True
            Catch ex As Exception
                MessageBox.Show("Si sono verificati degli errori durante il salvataggio. Verificare che il file di configurazione " + Me._logger._config_file + " sia accessibile ed effettuare test di scrittura del file di log.", "Errore salvataggio configurazioni", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Else
            MessageBox.Show("Si sono verificati degli errori durante il salvataggio. Verificare che il file di configurazione " + Me._logger._config_file + " sia accessibile ed effettuare test di scrittura del file di log.", "Errore salvataggio configurazioni", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        Return saved
    End Function

    Private Sub btnSalva_Click(sender As Object, e As EventArgs) Handles btnSalva.Click
        If Not Me.saveConfiguration() Then
        Else
            MessageBox.Show("Configurazione salvata correttamente", "Salvataggio riuscito", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnESC_Click(sender As Object, e As EventArgs) Handles btnESC.Click
        Me._logger.loadConfig()
        Me.Close()
    End Sub
End Class