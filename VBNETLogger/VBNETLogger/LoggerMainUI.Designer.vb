﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoggerMainUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoggerMainUI))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TabFiltri = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cB_date_filter = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpEnd = New System.Windows.Forms.DateTimePicker()
        Me.dtpStart = New System.Windows.Forms.DateTimePicker()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbRegex = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cb_Filter_regex = New System.Windows.Forms.CheckBox()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape2 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.rtb_logs = New System.Windows.Forms.RichTextBox()
        Me.TimerLogUpdate = New System.Windows.Forms.Timer(Me.components)
        Me.btnEraser = New System.Windows.Forms.Button()
        Me.btnSetting = New System.Windows.Forms.Button()
        Me.btnLog = New System.Windows.Forms.Button()
        Me.btnFilterMessage = New System.Windows.Forms.Button()
        Me.btnFilterError = New System.Windows.Forms.Button()
        Me.btnFilterInfo = New System.Windows.Forms.Button()
        Me.btnFilterWarning = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.TabFiltri.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.btnFilterMessage)
        Me.Panel1.Controls.Add(Me.btnFilterError)
        Me.Panel1.Controls.Add(Me.btnFilterInfo)
        Me.Panel1.Controls.Add(Me.btnFilterWarning)
        Me.Panel1.Controls.Add(Me.TabFiltri)
        Me.Panel1.Controls.Add(Me.ShapeContainer1)
        Me.Panel1.Location = New System.Drawing.Point(-1, -3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(792, 71)
        Me.Panel1.TabIndex = 6
        '
        'TabFiltri
        '
        Me.TabFiltri.Controls.Add(Me.TabPage1)
        Me.TabFiltri.Controls.Add(Me.TabPage2)
        Me.TabFiltri.Location = New System.Drawing.Point(3, 3)
        Me.TabFiltri.Name = "TabFiltri"
        Me.TabFiltri.SelectedIndex = 0
        Me.TabFiltri.Size = New System.Drawing.Size(596, 60)
        Me.TabFiltri.TabIndex = 12
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.cB_date_filter)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.dtpEnd)
        Me.TabPage1.Controls.Add(Me.dtpStart)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(588, 34)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Filtro data"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(33, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 20)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Da:"
        '
        'cB_date_filter
        '
        Me.cB_date_filter.AutoSize = True
        Me.cB_date_filter.Location = New System.Drawing.Point(12, 12)
        Me.cB_date_filter.Name = "cB_date_filter"
        Me.cB_date_filter.Size = New System.Drawing.Size(15, 14)
        Me.cB_date_filter.TabIndex = 11
        Me.cB_date_filter.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(-164, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 20)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Da:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(326, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(24, 20)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "A:"
        '
        'dtpEnd
        '
        Me.dtpEnd.Enabled = False
        Me.dtpEnd.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnd.Location = New System.Drawing.Point(351, 5)
        Me.dtpEnd.Name = "dtpEnd"
        Me.dtpEnd.Size = New System.Drawing.Size(230, 24)
        Me.dtpEnd.TabIndex = 12
        '
        'dtpStart
        '
        Me.dtpStart.Enabled = False
        Me.dtpStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStart.Location = New System.Drawing.Point(68, 6)
        Me.dtpStart.Name = "dtpStart"
        Me.dtpStart.Size = New System.Drawing.Size(230, 24)
        Me.dtpStart.TabIndex = 10
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.tbRegex)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.cb_Filter_regex)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(588, 34)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Filtro regex"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(478, 7)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(13, 20)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "/"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(94, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(13, 20)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "/"
        '
        'tbRegex
        '
        Me.tbRegex.Enabled = False
        Me.tbRegex.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbRegex.Location = New System.Drawing.Point(107, 5)
        Me.tbRegex.Name = "tbRegex"
        Me.tbRegex.Size = New System.Drawing.Size(371, 24)
        Me.tbRegex.TabIndex = 14
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(33, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 20)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Regex:"
        '
        'cb_Filter_regex
        '
        Me.cb_Filter_regex.AutoSize = True
        Me.cb_Filter_regex.Location = New System.Drawing.Point(16, 11)
        Me.cb_Filter_regex.Name = "cb_Filter_regex"
        Me.cb_Filter_regex.Size = New System.Drawing.Size(15, 14)
        Me.cb_Filter_regex.TabIndex = 12
        Me.cb_Filter_regex.UseVisualStyleBackColor = True
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape2})
        Me.ShapeContainer1.Size = New System.Drawing.Size(792, 71)
        Me.ShapeContainer1.TabIndex = 10
        Me.ShapeContainer1.TabStop = False
        '
        'RectangleShape2
        '
        Me.RectangleShape2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RectangleShape2.BorderColor = System.Drawing.Color.DarkGray
        Me.RectangleShape2.FillGradientColor = System.Drawing.Color.DeepSkyBlue
        Me.RectangleShape2.Location = New System.Drawing.Point(632, 21)
        Me.RectangleShape2.Name = "RectangleShape2"
        Me.RectangleShape2.Size = New System.Drawing.Size(150, 49)
        '
        'rtb_logs
        '
        Me.rtb_logs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtb_logs.BackColor = System.Drawing.Color.Black
        Me.rtb_logs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.rtb_logs.Font = New System.Drawing.Font("Gadugi", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtb_logs.ForeColor = System.Drawing.Color.Peru
        Me.rtb_logs.Location = New System.Drawing.Point(0, 65)
        Me.rtb_logs.Name = "rtb_logs"
        Me.rtb_logs.ReadOnly = True
        Me.rtb_logs.RightMargin = 700
        Me.rtb_logs.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
        Me.rtb_logs.Size = New System.Drawing.Size(787, 230)
        Me.rtb_logs.TabIndex = 7
        Me.rtb_logs.Text = ""
        '
        'TimerLogUpdate
        '
        '
        'btnEraser
        '
        Me.btnEraser.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEraser.BackColor = System.Drawing.Color.Peru
        Me.btnEraser.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown
        Me.btnEraser.FlatAppearance.BorderSize = 2
        Me.btnEraser.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEraser.Image = Global.VBNETLogger.My.Resources.Resources.eraser_icon_25
        Me.btnEraser.Location = New System.Drawing.Point(717, 161)
        Me.btnEraser.Name = "btnEraser"
        Me.btnEraser.Size = New System.Drawing.Size(45, 40)
        Me.btnEraser.TabIndex = 10
        Me.btnEraser.UseVisualStyleBackColor = False
        '
        'btnSetting
        '
        Me.btnSetting.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSetting.BackColor = System.Drawing.Color.Peru
        Me.btnSetting.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown
        Me.btnSetting.FlatAppearance.BorderSize = 2
        Me.btnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetting.Image = Global.VBNETLogger.My.Resources.Resources.setting_icon_25
        Me.btnSetting.Location = New System.Drawing.Point(717, 69)
        Me.btnSetting.Name = "btnSetting"
        Me.btnSetting.Size = New System.Drawing.Size(45, 40)
        Me.btnSetting.TabIndex = 9
        Me.btnSetting.UseVisualStyleBackColor = False
        '
        'btnLog
        '
        Me.btnLog.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLog.BackColor = System.Drawing.Color.Peru
        Me.btnLog.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown
        Me.btnLog.FlatAppearance.BorderSize = 2
        Me.btnLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLog.Image = Global.VBNETLogger.My.Resources.Resources.file_icon_32
        Me.btnLog.Location = New System.Drawing.Point(717, 115)
        Me.btnLog.Name = "btnLog"
        Me.btnLog.Size = New System.Drawing.Size(45, 40)
        Me.btnLog.TabIndex = 8
        Me.btnLog.UseVisualStyleBackColor = False
        '
        'btnFilterMessage
        '
        Me.btnFilterMessage.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilterMessage.BackColor = System.Drawing.Color.IndianRed
        Me.btnFilterMessage.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.btnFilterMessage.FlatAppearance.BorderSize = 2
        Me.btnFilterMessage.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed
        Me.btnFilterMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFilterMessage.Image = Global.VBNETLogger.My.Resources.Resources.message_icon_25
        Me.btnFilterMessage.Location = New System.Drawing.Point(636, 27)
        Me.btnFilterMessage.Name = "btnFilterMessage"
        Me.btnFilterMessage.Size = New System.Drawing.Size(34, 30)
        Me.btnFilterMessage.TabIndex = 16
        Me.btnFilterMessage.UseVisualStyleBackColor = False
        '
        'btnFilterError
        '
        Me.btnFilterError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilterError.BackColor = System.Drawing.Color.IndianRed
        Me.btnFilterError.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.btnFilterError.FlatAppearance.BorderSize = 2
        Me.btnFilterError.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed
        Me.btnFilterError.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFilterError.Image = Global.VBNETLogger.My.Resources.Resources.error_icon_25
        Me.btnFilterError.Location = New System.Drawing.Point(745, 27)
        Me.btnFilterError.Name = "btnFilterError"
        Me.btnFilterError.Size = New System.Drawing.Size(34, 30)
        Me.btnFilterError.TabIndex = 15
        Me.btnFilterError.UseVisualStyleBackColor = False
        '
        'btnFilterInfo
        '
        Me.btnFilterInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilterInfo.BackColor = System.Drawing.Color.IndianRed
        Me.btnFilterInfo.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.btnFilterInfo.FlatAppearance.BorderSize = 2
        Me.btnFilterInfo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed
        Me.btnFilterInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFilterInfo.Image = Global.VBNETLogger.My.Resources.Resources.info_icon_25
        Me.btnFilterInfo.Location = New System.Drawing.Point(672, 27)
        Me.btnFilterInfo.Name = "btnFilterInfo"
        Me.btnFilterInfo.Size = New System.Drawing.Size(34, 30)
        Me.btnFilterInfo.TabIndex = 14
        Me.btnFilterInfo.UseVisualStyleBackColor = False
        '
        'btnFilterWarning
        '
        Me.btnFilterWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilterWarning.BackColor = System.Drawing.Color.IndianRed
        Me.btnFilterWarning.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.btnFilterWarning.FlatAppearance.BorderSize = 2
        Me.btnFilterWarning.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed
        Me.btnFilterWarning.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFilterWarning.Image = Global.VBNETLogger.My.Resources.Resources.warning_icon_25
        Me.btnFilterWarning.Location = New System.Drawing.Point(708, 27)
        Me.btnFilterWarning.Name = "btnFilterWarning"
        Me.btnFilterWarning.Size = New System.Drawing.Size(34, 30)
        Me.btnFilterWarning.TabIndex = 13
        Me.btnFilterWarning.UseVisualStyleBackColor = False
        '
        'LoggerMainUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(787, 295)
        Me.Controls.Add(Me.btnEraser)
        Me.Controls.Add(Me.btnSetting)
        Me.Controls.Add(Me.btnLog)
        Me.Controls.Add(Me.rtb_logs)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(803, 333)
        Me.Name = "LoggerMainUI"
        Me.Text = "Logger - Main"
        Me.Panel1.ResumeLayout(False)
        Me.TabFiltri.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents rtb_logs As System.Windows.Forms.RichTextBox
    Friend WithEvents btnLog As System.Windows.Forms.Button
    Friend WithEvents RectangleShape2 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents btnSetting As System.Windows.Forms.Button
    Friend WithEvents TimerLogUpdate As System.Windows.Forms.Timer
    Friend WithEvents TabFiltri As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cB_date_filter As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnFilterMessage As System.Windows.Forms.Button
    Friend WithEvents btnFilterError As System.Windows.Forms.Button
    Friend WithEvents btnFilterInfo As System.Windows.Forms.Button
    Friend WithEvents btnFilterWarning As System.Windows.Forms.Button
    Friend WithEvents tbRegex As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cb_Filter_regex As System.Windows.Forms.CheckBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnEraser As System.Windows.Forms.Button
End Class
