﻿Imports System.Text
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Drawing
Imports System.Windows.Forms

Public Class LoggerMainUI
    Private log_buffer As String = ""
    Private _filter_log_type_bitmask(VBLogger.LOG_TYPES._LOG_TYPE_LAST) As Integer
    Private _logger As VBLogger

    Public Sub New(ByRef logger As VBLogger)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._logger = logger
    End Sub

    Private Sub Filter()
        Me._logger.SetFilterDateRange({dtpStart.Value, dtpEnd.Value})

        rtb_logs.Text = ""
        For Each log_line As String In Me.log_buffer.Split(Chr(13))
            ' Struttura log_line: [PRIORITY][TYPE][DATE] LOG TEXT <| Eccezione generata: EXCEPTION MESSAGE>       dove <...> = opzionale
            If Not (log_line = "") Then
                rtb_logs.Text += VBLogger.FilterLog(log_line, IIf(cB_date_filter.Checked, {dtpStart.Value, dtpEnd.Value}, Nothing), tbRegex.Text.Trim, Me._logger._ui_log_priority, Me._filter_log_type_bitmask) + Chr(13)
            End If
        Next

        rtb_logs.SelectionStart = rtb_logs.Text.Length
        rtb_logs.ScrollToCaret()
    End Sub

    Private Sub SetLogTypes(Optional ByVal bitmask As Integer() = Nothing)
        ' Abilito le tipologie di log
        If IsNothing(bitmask) Then
            bitmask = {1, 1, 1, 1}
        End If

        Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX) = bitmask(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX)
        Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_INFO) = bitmask(VBLogger.LOG_TYPES.LOG_INFO)
        Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_WARNING) = bitmask(VBLogger.LOG_TYPES.LOG_WARNING)
        Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_ERROR) = bitmask(VBLogger.LOG_TYPES.LOG_ERROR)

        Me._logger.SetFilterLogBitMask(bitmask)
    End Sub

    Private Sub LoggerMainUI_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        VBLoggerFactory.Logger.loadConfig()

        Me.TimerLogUpdate.Interval = VBLoggerFactory.Logger._ui_update_frequency_ms

        Me.SetLogTypes()

        Me.Filter()

        Me.TimerLogUpdate.Start()
    End Sub

    Private Sub TimerLogUpdate_Tick(sender As Object, e As EventArgs) Handles TimerLogUpdate.Tick
        Dim log As String = VBLogger.FilterLog(New StringBuilder(VBLoggerFactory.Logger.GetUILog()).ToString, IIf(cB_date_filter.Checked, {dtpStart.Value, dtpEnd.Value}, Nothing), tbRegex.Text.Trim, Me._logger._ui_log_priority, Me._filter_log_type_bitmask).Trim

        If Not (log = "") Then
            log_buffer += log + Chr(13)
            rtb_logs.AppendText(log + Chr(13))
            rtb_logs.SelectionStart = rtb_logs.Text.Length
            rtb_logs.ScrollToCaret()
        End If
    End Sub

    Private Sub cB_date_filter_CheckedChanged(sender As Object, e As EventArgs) Handles cB_date_filter.CheckedChanged
        Me.dtpStart.Enabled = Me.cB_date_filter.Checked
        Me.dtpEnd.Enabled = Me.cB_date_filter.Checked
    End Sub

    Private Sub dtpStart_ValueChanged(sender As Object, e As EventArgs) Handles dtpStart.ValueChanged
        Me.Filter()
    End Sub

    Private Sub Panel1_Paint(sender As Object, e As Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub btnFilterMessage_Click(sender As Object, e As EventArgs) Handles btnFilterMessage.Click
        Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX) = IIf(_filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX) > 0, _filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX) - 1, _filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX) + 1)

        Me.SetLogTypes(Me._filter_log_type_bitmask)

        Me.btnFilterMessage.BackColor = IIf(Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX) = 1, Color.IndianRed, Color.FromName("ButtonFace"))

        'VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX, "test message box")
        'VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_INFO, "test info")
        'VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_WARNING, "test WARNING")
        'VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_ERROR, "test error")
    End Sub

    Private Sub btnFilterInfo_Click(sender As Object, e As EventArgs) Handles btnFilterInfo.Click
        Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_INFO) = IIf(_filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_INFO) > 0, _filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_INFO) - 1, _filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_INFO) + 1)

        Me.SetLogTypes(Me._filter_log_type_bitmask)

        Me.btnFilterInfo.BackColor = IIf(Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_INFO) = 1, Color.IndianRed, Color.FromName("ButtonFace"))

    End Sub

    Private Sub btnFilterWarning_Click(sender As Object, e As EventArgs) Handles btnFilterWarning.Click
        Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_WARNING) = IIf(_filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_WARNING) > 0, _filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_WARNING) - 1, _filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_WARNING) + 1)

        Me.SetLogTypes(Me._filter_log_type_bitmask)

        Me.btnFilterWarning.BackColor = IIf(Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_WARNING) = 1, Color.IndianRed, Color.FromName("ButtonFace"))

    End Sub

    Private Sub btnFilterError_Click(sender As Object, e As EventArgs) Handles btnFilterError.Click
        Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_ERROR) = IIf(_filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_ERROR) > 0, _filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_ERROR) - 1, _filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_ERROR) + 1)

        Me.SetLogTypes(Me._filter_log_type_bitmask)

        Me.btnFilterError.BackColor = IIf(Me._filter_log_type_bitmask(VBLogger.LOG_TYPES.LOG_ERROR) = 1, Color.IndianRed, Color.FromName("ButtonFace"))

    End Sub

    Private Sub btnLog_Click(sender As Object, e As EventArgs) Handles btnLog.Click
        VBLoggerFactory.Logger.OpenLogFile()
    End Sub

    Private Sub LoggerMainUI_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Me._logger.CloseUI()
    End Sub

    Private Sub btnEraser_Click(sender As Object, e As EventArgs) Handles btnEraser.Click
        rtb_logs.Text = ""
    End Sub

    Private Sub dtpEnd_ValueChanged(sender As Object, e As EventArgs) Handles dtpEnd.ValueChanged
        Me.Filter()
    End Sub

    Private Sub tbRegex_TextChanged(sender As Object, e As EventArgs) Handles tbRegex.TextChanged
        Me.Filter()
    End Sub

    Private Sub btnSetting_Click(sender As Object, e As EventArgs) Handles btnSetting.Click
        Dim configForm As LoggerConfigUI = New LoggerConfigUI(VBLoggerFactory.Logger)

        configForm.Show()
    End Sub

    Private Sub cb_Filter_regex_CheckedChanged(sender As Object, e As EventArgs) Handles cb_Filter_regex.CheckedChanged
        Me.tbRegex.Enabled = Me.cb_Filter_regex.Checked
    End Sub
End Class