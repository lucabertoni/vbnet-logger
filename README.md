# vbnet-logger

Logger written in VB .NET

![Main UI](Doc/screenshots/MainUI.PNG)

## Technologies involved
VisualBasic .NET Framework 4.5  
Built as/Application Type: Class Library

## How to use vbnet-logger in your applications
1. Get a copy of the source
2. Copy the DLL (vbnet-logger\VBNETLogger\VBNETLogger\bin\Debug\VBNETLogger.dll) in your project directory  
![Copia DLL](Doc/screenshots/Installazione_Copia_DLL.PNG)
3. Open your project in VisualStudio and add the DLL to the references  
![Aggiunta reference](Doc/screenshots/Installazione_Aggiunta_Reference.PNG)  
![Aggiunta reference](Doc/screenshots/Installazione_Aggiunta_Reference_2.PNG)  
4. Import classes in your code  
![Aggiunta reference](Doc/screenshots/Installazione_Aggiunta_Reference_2_import.PNG)
5. Use it  
    There are four types of logs: message, info, warning and error. 
    Here is a call to the method _Log()_ of the class _Logger_, instantiated through the use of _VBLoggerFactory_ Static class which assures to get one Logger object for software instance (basic its functioning upon Shared methods and attributes)  
    ```visualbasic
    VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_MESSAGE_BOX, "Copia completata correttamente", Nothing, "Copia completata", MessageBoxButtons.OK, MessageBoxIcon.Information)


    VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_INFO, "DEFECTS_GRABBER -> LoadAllClients -> " + String.Format("Caricato client ", clients_list.list(clients_list.list.Count – 1).label))

	
	VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_WARNING, "DEFECTS_GRABBER -> 	ScanClient -> Variabile file è uguale a Nothing")


    VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_ERROR, String.Format("DEFECTS_GRABBER -> ScanClient -> Errore lettura cartella difetti client [{0}]{1}", client.label, file))

    ```  
	It is also possibile to include an exception into the log  
    ```visualbasic
    Try
        cn.Open()
	    cmd.ExecuteNonQuery()
        cn.Close()
	Catch ex As Exception
        VBLoggerFactory.Logger.Log(VBLogger.LOG_TYPES.LOG_ERROR, "DEFECTS_GRABBER -> AddScanFile -> Errore esecuzione query.", ex)
    End Try
    ```  
6. Read the logs  
   There are two ways to open the UI to read the logs:  
   * Using the method _ShowUI()_ of the class _Logger_  

    ```visualbasic
    VBLoggerFactory.Logger.ShowUI()

    ```
   * Using the _OpenLogBTN_ a custom control composed by a simple button which calls the method described above and open the UI  
    ![Aggiunta reference](Doc/screenshots/Installazione_Aggiunta_Reference_4_button.PNG)   
    ![Aggiunta reference](Doc/screenshots/Installazione_Aggiunta_Reference_5_button_2.PNG)  
    ![Aggiunta reference](Doc/screenshots/Installazione_Aggiunta_Reference_6_button_3.PNG)  

7. User interface  

    _Main form_   
    ![Main UI](Doc/screenshots/MainUI_info.PNG)  
    1. Log filters. You can filter by a range of dates or by using regular expressions  
    2. Checkboxes which defines the printing states of the different log typologies: message, info, warning, error  
    3. Where the logs are printed  
    4. Useful buttons  
        * Settings  
        * Open log text file  
        * Clear log window  

    _Settings Form_  
    ![Settings UI](Doc/screenshots/SettingsUI.png)  

    Through the Settings Form it is possible to setup the path of the log file, test if it is writable, and set the update interval of the UI.  
